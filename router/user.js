const Router = require("../util/router");

const userList = [
  { id: 0, name: "Dima", age: 25 },
  { id: 1, name: "Vasya", age: 17 },
  { id: 2, name: "Petya", age: 213 },
  { id: 3, name: "Sasha", age: 32 },
  { id: 4, name: "Kolya", age: 54 },
  { id: 5, name: "Igor", age: 56 },
  { id: 6, name: "Ura", age: 23 },
  { id: 7, name: "Vasya", age: 45 },
];

function subscribeUserRouter(server) {
  const router = new Router(server, "/user");

  router.get("", (req, res) => {
    res.setHeader("Content-Type", "application/json");
    res.statusCode = 200;
    res.end(JSON.stringify(userList));
  });

  router.post("", (req, res) => {
    const userToCreate = { id: userList.length + 1, ...JSON.parse(req.body) };
    userList.push(userToCreate);

    res.setHeader("Content-Type", "application/json");
    res.statusCode = 201;
    res.end(JSON.stringify(userToCreate));
  });

  router.delete("", (req, res) => {
    userList.splice(
      userList.findIndex((user) => user.id === req.body),
      1
    );
    res.statusCode = 204;
    res.end();
  });

  router.options("", (req, res) => {
    res.statusCode = 200;
    res.end();
  });
}

module.exports = {
  subscribeUserRouter,
};
